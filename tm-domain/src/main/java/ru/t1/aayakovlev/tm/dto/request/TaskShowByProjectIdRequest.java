package ru.t1.aayakovlev.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public final class TaskShowByProjectIdRequest extends AbstractUserRequest {

    @Nullable
    private String projectId;

    public TaskShowByProjectIdRequest(@Nullable final String token) {
        super(token);
    }

}
