package ru.t1.aayakovlev.tm;

import lombok.SneakyThrows;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.listener.EntityListener;
import ru.t1.aayakovlev.tm.service.LoggerService;

import javax.jms.*;

import static ru.t1.aayakovlev.tm.constant.ApplicationConstant.MONGO_URL;
import static ru.t1.aayakovlev.tm.constant.ApplicationConstant.QUEUE;

public final class Application {

    @SneakyThrows
    public static void main(@Nullable final String[] args) {
        @NotNull final LoggerService loggerService = new LoggerService();
        @NotNull final EntityListener entityListener = new EntityListener(loggerService);
        @NotNull final ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(MONGO_URL);
        @NotNull final Connection connection = connectionFactory.createConnection();
        connection.start();
        @NotNull final Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        @NotNull final Queue destination = session.createQueue(QUEUE);
        @NotNull final MessageConsumer messageConsumer = session.createConsumer(destination);
        messageConsumer.setMessageListener(entityListener);
    }

}
