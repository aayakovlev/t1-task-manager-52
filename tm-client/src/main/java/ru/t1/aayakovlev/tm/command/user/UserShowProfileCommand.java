package ru.t1.aayakovlev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.dto.model.UserDTO;
import ru.t1.aayakovlev.tm.dto.request.UserShowProfileRequest;
import ru.t1.aayakovlev.tm.dto.response.UserShowProfileResponse;
import ru.t1.aayakovlev.tm.exception.AbstractException;

public final class UserShowProfileCommand extends AbstractUserCommand {

    @NotNull
    public static final String DESCRIPTION = "Show user profile.";

    @NotNull
    public static final String NAME = "user-show-profile";

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[SHOW USER PROFILE]");

        @NotNull final UserShowProfileRequest request = new UserShowProfileRequest(getToken());
        @Nullable final UserShowProfileResponse response = getAuthEndpoint().profile(request);
        @Nullable final UserDTO user = response.getUser();
        if (user == null) return;

        showUser(user);
        System.out.println("First name: " + user.getFirstName());
        System.out.println("Middle name: " + user.getMiddleName());
        System.out.println("Last name: " + user.getLastName());
        System.out.println("Role: " + user.getRole().getDisplayName());
    }

}
