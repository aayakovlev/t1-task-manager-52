package ru.t1.aayakovlev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.dto.model.TaskDTO;
import ru.t1.aayakovlev.tm.dto.request.TaskShowByProjectIdRequest;
import ru.t1.aayakovlev.tm.dto.response.TaskShowByProjectIdResponse;
import ru.t1.aayakovlev.tm.exception.AbstractException;

import java.util.Collections;
import java.util.List;

import static ru.t1.aayakovlev.tm.util.TerminalUtil.nextLine;

public final class TaskShowByProjectIdCommand extends AbstractTaskCommand {

    @NotNull
    public static final String DESCRIPTION = "Show tasks by project id.";

    @NotNull
    public static final String NAME = "task-show-by-project-id";

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[SHOW TASKS BY PROJECT ID]");
        System.out.print("Enter projectId: ");
        @NotNull final String projectId = nextLine();

        @NotNull final TaskShowByProjectIdRequest request = new TaskShowByProjectIdRequest(getToken());
        request.setProjectId(projectId);
        @Nullable final TaskShowByProjectIdResponse response = getTaskEndpoint().showTasksByProjectId(request);
        if (response.getTasks() == null) response.setTasks(Collections.emptyList());
        @NotNull final List<TaskDTO> tasks = response.getTasks();

        renderTasks(tasks);
    }

}
