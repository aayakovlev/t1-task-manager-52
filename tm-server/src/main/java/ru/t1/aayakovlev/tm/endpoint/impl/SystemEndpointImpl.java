package ru.t1.aayakovlev.tm.endpoint.impl;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.dto.request.ServerAboutRequest;
import ru.t1.aayakovlev.tm.dto.request.ServerHostnameRequest;
import ru.t1.aayakovlev.tm.dto.request.ServerVersionRequest;
import ru.t1.aayakovlev.tm.dto.response.ServerAboutResponse;
import ru.t1.aayakovlev.tm.dto.response.ServerHostnameResponse;
import ru.t1.aayakovlev.tm.dto.response.ServerVersionResponse;
import ru.t1.aayakovlev.tm.endpoint.SystemEndpoint;
import ru.t1.aayakovlev.tm.service.PropertyService;
import ru.t1.aayakovlev.tm.service.ServiceLocator;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.net.InetAddress;

@NoArgsConstructor
@WebService(endpointInterface = "ru.t1.aayakovlev.tm.endpoint.SystemEndpoint")
public final class SystemEndpointImpl extends AbstractEndpoint implements SystemEndpoint {

    public SystemEndpointImpl(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    @WebMethod
    public ServerAboutResponse getAbout(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ServerAboutRequest request
    ) {
        @NotNull final PropertyService propertyService = getServiceLocator().getPropertyService();
        @NotNull final ServerAboutResponse response = new ServerAboutResponse();
        response.setEmail(propertyService.getAuthorEmail());
        response.setName(propertyService.getAuthorName());
        response.setGitBranch(propertyService.getGitBranch());
        response.setGitCommitId(propertyService.getGitCommitId());
        response.setGitCommitMessage(propertyService.getGitCommitMessage());
        response.setGitCommitterEmail(propertyService.getGitCommitterEmail());
        response.setGitCommitterName(propertyService.getGitCommitterName());
        response.setGitCommitTime(propertyService.getGitCommitTime());
        return response;
    }

    @NotNull
    @Override
    @WebMethod
    public ServerVersionResponse getVersion(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ServerVersionRequest request
    ) {
        @NotNull final PropertyService propertyService = getServiceLocator().getPropertyService();
        @NotNull final ServerVersionResponse response = new ServerVersionResponse();
        response.setVersion(propertyService.getApplicationVersion());
        return response;
    }

    @NotNull
    @Override
    @SneakyThrows
    public ServerHostnameResponse getHostname(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ServerHostnameRequest request
    ) {
        @NotNull final String hostname = InetAddress.getLocalHost().getHostName();
        @NotNull final ServerHostnameResponse response = new ServerHostnameResponse();
        response.setHostname(hostname);
        return response;
    }

}
