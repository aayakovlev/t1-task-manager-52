package ru.t1.aayakovlev.tm.repository.model.impl;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.comparator.CreatedComparator;
import ru.t1.aayakovlev.tm.enumerated.Role;
import ru.t1.aayakovlev.tm.model.User;
import ru.t1.aayakovlev.tm.repository.model.UserRepository;

import javax.persistence.EntityManager;
import java.util.Comparator;

public final class UserRepositoryImpl extends AbstractBaseRepository<User> implements UserRepository {

    public UserRepositoryImpl(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    protected Class<User> getClazz() {
        return User.class;
    }

    @NotNull
    @Override
    protected String getSortColumnName(@NotNull final Comparator comparator) {
        if (comparator == CreatedComparator.INSTANCE) return "created";
        return "created";

    }

    @NotNull
    @Override
    public User create(@NotNull final String login, @NotNull final String password) {
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(password);
        return save(user);
    }

    @NotNull
    @Override
    public User create(@NotNull final String login, @NotNull final String password, @Nullable final String email) {
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setEmail(email);
        user.setPasswordHash(password);
        return save(user);
    }

    @NotNull
    @Override
    public User create(@NotNull final String login, @NotNull final String password, @Nullable final Role role) {
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(password);
        user.setRole(role);
        return save(user);
    }

    @Nullable
    @Override
    public User findByLogin(@NotNull final String login) {
        @NotNull final String query = "from " + getClazz().getSimpleName() + " e " +
                "where e.login = :login";
        return entityManager.createQuery(query, getClazz())
                .setParameter("login", login)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public User findByEmail(@NotNull final String email) {
        @NotNull final String query = "from " + getClazz().getSimpleName() + " e " +
                "where e.email = :email";
        return entityManager.createQuery(query, getClazz())
                .setParameter("email", email)
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst()
                .orElse(null);
    }

    @Override
    public boolean isLoginExists(@NotNull final String login) {
        return findByLogin(login) != null;
    }

    @Override
    public boolean isEmailExists(@NotNull final String email) {
        return findByEmail(email) != null;
    }

}
