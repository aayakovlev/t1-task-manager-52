package ru.t1.aayakovlev.tm.service.impl;

import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.resource.ClassLoaderResourceAccessor;
import lombok.SneakyThrows;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.dto.model.ProjectDTO;
import ru.t1.aayakovlev.tm.dto.model.SessionDTO;
import ru.t1.aayakovlev.tm.dto.model.TaskDTO;
import ru.t1.aayakovlev.tm.dto.model.UserDTO;
import ru.t1.aayakovlev.tm.model.Project;
import ru.t1.aayakovlev.tm.model.Session;
import ru.t1.aayakovlev.tm.model.Task;
import ru.t1.aayakovlev.tm.model.User;
import ru.t1.aayakovlev.tm.service.ConnectionService;
import ru.t1.aayakovlev.tm.service.DatabaseProperty;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import static org.hibernate.cfg.Environment.*;
import static ru.t1.aayakovlev.tm.constant.ApplicationConstant.LIQUIBASE_CHANGELOG_FILENAME;

public final class ConnectionServiceImpl implements ConnectionService {

    @NotNull
    private static final ClassLoaderResourceAccessor ACCESSOR = new ClassLoaderResourceAccessor();

    @NotNull
    private static Database DATABASE;

    @NotNull
    private final DatabaseProperty databaseProperty;

    @NotNull
    private final EntityManagerFactory entityManagerFactory;

    public ConnectionServiceImpl(@NotNull final DatabaseProperty databaseProperty) {
        this.databaseProperty = databaseProperty;
        this.entityManagerFactory = factory();
        initLiquibaseConnection();
    }

    public void close() {
        getEntityManager().close();
        entityManagerFactory.close();
    }

    @NotNull
    @Override
    public Liquibase getLiquibase() {
        return new Liquibase(LIQUIBASE_CHANGELOG_FILENAME, ACCESSOR, DATABASE);
    }

    @NotNull
    @Override
    @SneakyThrows
    public EntityManager getEntityManager() {
        return entityManagerFactory.createEntityManager();
    }

    @NotNull
    private EntityManagerFactory factory() {
        @NotNull final Map<String, String> settings = new HashMap<>();
        settings.put(DRIVER, databaseProperty.getDatabaseDriver());
        settings.put(URL, databaseProperty.getDatabaseURL());
        settings.put(USER, databaseProperty.getDatabaseUser());
        settings.put(PASS, databaseProperty.getDatabasePassword());
        settings.put(DIALECT, databaseProperty.getDatabaseDialect());
        settings.put(HBM2DDL_AUTO, databaseProperty.getDatabaseHBM2DLL());
        settings.put(SHOW_SQL, databaseProperty.getDataBaseShowSql());
        settings.put(FORMAT_SQL, databaseProperty.getDataBaseFormatSql());
        settings.put(DEFAULT_SCHEMA, databaseProperty.getDataBaseSchema());

        settings.put(USE_SECOND_LEVEL_CACHE, databaseProperty.getUseSecondCache());
        settings.put(USE_QUERY_CACHE, databaseProperty.getUseQueryCache());
        settings.put(USE_MINIMAL_PUTS, databaseProperty.getUseMinimalPuts());
        settings.put(CACHE_REGION_PREFIX, databaseProperty.getUseRegionPrefix());
        settings.put(CACHE_REGION_FACTORY, databaseProperty.getFactoryClass());
        settings.put(CACHE_PROVIDER_CONFIG, databaseProperty.getHZConfFile());

        @NotNull final StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        @NotNull final StandardServiceRegistry registry = registryBuilder.build();
        @NotNull final MetadataSources sources = new MetadataSources(registry);
        sources.addAnnotatedClass(ProjectDTO.class);
        sources.addAnnotatedClass(TaskDTO.class);
        sources.addAnnotatedClass(SessionDTO.class);
        sources.addAnnotatedClass(UserDTO.class);
        sources.addAnnotatedClass(Project.class);
        sources.addAnnotatedClass(Task.class);
        sources.addAnnotatedClass(Session.class);
        sources.addAnnotatedClass(User.class);
        @NotNull final Metadata metadata = sources.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }

    @SneakyThrows
    private void initLiquibaseConnection() {
        @NotNull final Connection connection = getConnection();
        @NotNull final JdbcConnection jdbcConnection = new JdbcConnection(connection);
        DATABASE = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(jdbcConnection);
    }

    @NotNull
    private Connection getConnection() throws SQLException {
        return DriverManager.getConnection(
                databaseProperty.getDatabaseURL(),
                databaseProperty.getDatabaseUser(),
                databaseProperty.getDatabasePassword()
        );
    }

}
