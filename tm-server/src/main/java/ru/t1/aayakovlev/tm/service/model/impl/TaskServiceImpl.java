package ru.t1.aayakovlev.tm.service.model.impl;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.enumerated.Sort;
import ru.t1.aayakovlev.tm.enumerated.Status;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.auth.AuthenticationException;
import ru.t1.aayakovlev.tm.exception.field.*;
import ru.t1.aayakovlev.tm.model.Task;
import ru.t1.aayakovlev.tm.repository.model.TaskRepository;
import ru.t1.aayakovlev.tm.repository.model.impl.TaskRepositoryImpl;
import ru.t1.aayakovlev.tm.service.ConnectionService;
import ru.t1.aayakovlev.tm.service.model.TaskService;

import javax.persistence.EntityManager;
import java.util.List;

public final class TaskServiceImpl extends AbstractExtendedService<Task, TaskRepository>
        implements TaskService {

    public TaskServiceImpl(@NotNull final ConnectionService connectionService) {
        super(connectionService);
    }

    @Override
    protected @NotNull TaskRepository getRepository(@NotNull final EntityManager entityManager) {
        return new TaskRepositoryImpl(entityManager);
    }

    @NotNull
    @Override
    public Task create(
            @Nullable final String userId,
            @Nullable final String name
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable Task resultEntity;
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final TaskRepository entityRepository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            resultEntity = entityRepository.create(userId, name);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return resultEntity;
    }

    @NotNull
    @Override
    public Task create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable Task resultEntity;
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final TaskRepository entityRepository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            resultEntity = entityRepository.create(userId, name, description);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return resultEntity;
    }

    @NotNull
    @Override
    public Task changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusEmptyException();
        @Nullable Task resultTask = findById(userId, id);
        resultTask.setStatus(status);
        resultTask = update(resultTask);
        return resultTask;
    }

    @NotNull
    @Override
    public List<Task> findAll(
            @Nullable final String userId,
            @Nullable final Sort sort
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (sort == null) return findAll(userId);
        return findAll(userId, sort.getComparator());
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(
            @Nullable final String userId,
            @Nullable final String projectId
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        @Nullable List<Task> resultEntities;
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final TaskRepository entityRepository = getRepository(entityManager);
            resultEntities = entityRepository.findAllByProjectId(userId, projectId);
        } finally {
            entityManager.close();
        }
        return resultEntities;

    }

    @NotNull
    @Override
    public Task update(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable Task model = findById(userId, id);
        model.setName(name);
        model.setDescription(description);
        return update(userId, model);
    }

}
