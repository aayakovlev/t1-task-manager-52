package ru.t1.aayakovlev.tm.repository;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.aayakovlev.tm.MeasureHelper;
import ru.t1.aayakovlev.tm.dto.model.UserDTO;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.marker.UnitCategory;
import ru.t1.aayakovlev.tm.migration.AbstractSchemeTest;
import ru.t1.aayakovlev.tm.repository.dto.UserDTORepository;
import ru.t1.aayakovlev.tm.repository.dto.impl.UserDTORepositoryImpl;
import ru.t1.aayakovlev.tm.service.ConnectionService;
import ru.t1.aayakovlev.tm.service.PropertyService;
import ru.t1.aayakovlev.tm.service.dto.UserDTOService;
import ru.t1.aayakovlev.tm.service.dto.impl.UserDTOServiceImpl;
import ru.t1.aayakovlev.tm.service.impl.ConnectionServiceImpl;
import ru.t1.aayakovlev.tm.service.impl.PropertyServiceImpl;

import javax.persistence.EntityManager;
import java.util.List;

import static ru.t1.aayakovlev.tm.constant.ApplicationConstant.LIQUIBASE_CHANGELOG_FILENAME;
import static ru.t1.aayakovlev.tm.constant.UserTestConstant.*;

@Category(UnitCategory.class)
public final class UserRepositoryImplTest extends AbstractSchemeTest {

    @NotNull
    private static PropertyService propertyService;

    @NotNull
    private static ConnectionService connectionService;

    @NotNull
    private static EntityManager entityManager;

    @NotNull
    private static UserDTORepository repository;

    @NotNull
    private static UserDTOService service;

    @BeforeClass
    public static void initConnectionService() throws LiquibaseException {
        @NotNull final Liquibase liquibase = liquibase(LIQUIBASE_CHANGELOG_FILENAME);
        liquibase.dropAll();
        liquibase.update("scheme");

        propertyService = new PropertyServiceImpl();
        connectionService = new ConnectionServiceImpl(propertyService);
        entityManager = connectionService.getEntityManager();
        repository = new UserDTORepositoryImpl(entityManager);
        service = new UserDTOServiceImpl(connectionService, propertyService);
    }

    @AfterClass
    public static void destroyConnection() {
        connectionService.close();
    }

    @Before
    public void initData() throws AbstractException {
        service.save(COMMON_USER_ONE);
        service.save(COMMON_USER_TWO);
    }

    @After
    public void after() throws AbstractException {
        service.clear();
    }

    @Test
    public void When_FindByIdExistsUser_Expect_ReturnUser() {
        @Nullable final UserDTO user = repository.findById(COMMON_USER_ONE.getId());
        Assert.assertNotNull(user);
        Assert.assertEquals(COMMON_USER_ONE.getEmail(), user.getEmail());
        Assert.assertEquals(COMMON_USER_ONE.getFirstName(), user.getFirstName());
        Assert.assertEquals(COMMON_USER_ONE.getLastName(), user.getLastName());
        Assert.assertEquals(COMMON_USER_ONE.getMiddleName(), user.getMiddleName());
        Assert.assertEquals(COMMON_USER_ONE.getLogin(), user.getLogin());
    }

    @Test
    public void When_FindByIdExistsUser_Expect_ReturnNull() {
        @Nullable final UserDTO user = repository.findById(USER_ID_NOT_EXISTED);
        Assert.assertNull(user);
    }

    @Test
    @SneakyThrows
    public void When_SaveNotNullUser_Expect_ReturnUser() {
        MeasureHelper.measure(entityManager, () -> repository.save(ADMIN_USER_THREE));
        @Nullable final UserDTO user = repository.findById(ADMIN_USER_THREE.getId());
        Assert.assertNotNull(user);
        Assert.assertEquals(ADMIN_USER_THREE.getId(), user.getId());
    }

    @Test
    public void When_FindAllUserId_Expected_ReturnListUsers() {
        final List<UserDTO> users = repository.findAll();
        for (int i = 0; i < users.size(); i++) {
            Assert.assertEquals(COMMON_USER_SORTED_LIST.get(i).getId(), users.get(i).getId());
        }
    }

    @Test
    @SneakyThrows
    public void When_RemoveExistedUser_Expect_ReturnUser() {
        MeasureHelper.measure(entityManager, () -> repository.save(ADMIN_USER_ONE));
        Assert.assertNotNull(repository.findById(ADMIN_USER_ONE.getId()));
        MeasureHelper.measure(entityManager, () -> repository.removeById(ADMIN_USER_ONE.getId()));
        Assert.assertNotNull(repository.findById(ADMIN_USER_ONE.getId()));
    }

    @Test
    @SneakyThrows
    public void When_RemoveAll_Expect_NullUsers() {
        MeasureHelper.measure(entityManager, () -> repository.save(ADMIN_USER_ONE));
        MeasureHelper.measure(entityManager, () -> repository.save(ADMIN_USER_TWO));
        MeasureHelper.measure(entityManager, () -> repository.clear());
        Assert.assertNotNull(repository.findById(ADMIN_USER_ONE.getId()));
        Assert.assertNotNull(repository.findById(ADMIN_USER_TWO.getId()));
    }

    @Test
    public void When_RemoveByIdNotExistedUser_Expect_ThrowsEntityNotFoundException() {
        MeasureHelper.measure(entityManager, () -> repository.removeById(USER_ID_NOT_EXISTED));
    }

}
