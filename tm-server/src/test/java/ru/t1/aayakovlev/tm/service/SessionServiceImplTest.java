package ru.t1.aayakovlev.tm.service;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.aayakovlev.tm.dto.model.SessionDTO;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.EntityNotFoundException;
import ru.t1.aayakovlev.tm.marker.UnitCategory;
import ru.t1.aayakovlev.tm.migration.AbstractSchemeTest;
import ru.t1.aayakovlev.tm.service.dto.SessionDTOService;
import ru.t1.aayakovlev.tm.service.dto.UserDTOService;
import ru.t1.aayakovlev.tm.service.dto.impl.SessionDTOServiceImpl;
import ru.t1.aayakovlev.tm.service.dto.impl.UserDTOServiceImpl;
import ru.t1.aayakovlev.tm.service.impl.ConnectionServiceImpl;
import ru.t1.aayakovlev.tm.service.impl.PropertyServiceImpl;

import static ru.t1.aayakovlev.tm.constant.ApplicationConstant.LIQUIBASE_CHANGELOG_FILENAME;
import static ru.t1.aayakovlev.tm.constant.SessionTestConstant.*;
import static ru.t1.aayakovlev.tm.constant.UserTestConstant.ADMIN_USER_ONE;
import static ru.t1.aayakovlev.tm.constant.UserTestConstant.COMMON_USER_ONE;

@Category(UnitCategory.class)
public final class SessionServiceImplTest extends AbstractSchemeTest {

    @NotNull
    private static PropertyService propertyService;

    @NotNull
    private static ConnectionService connectionService;

    @NotNull
    private static SessionDTOService service;

    @NotNull
    private static UserDTOService userService;

    @BeforeClass
    public static void initConnectionService() throws LiquibaseException {
        @NotNull final Liquibase liquibase = liquibase(LIQUIBASE_CHANGELOG_FILENAME);
        liquibase.dropAll();
        liquibase.update("scheme");

        propertyService = new PropertyServiceImpl();
        connectionService = new ConnectionServiceImpl(propertyService);
        service = new SessionDTOServiceImpl(connectionService);
        userService = new UserDTOServiceImpl(connectionService, propertyService);
    }

    @AfterClass
    public static void destroyConnection() {
        connectionService.close();
    }

    @Before
    public void initData() throws AbstractException {
        userService.save(COMMON_USER_ONE);
        userService.save(ADMIN_USER_ONE);
        service.save(SESSION_USER_ONE);
        service.save(SESSION_USER_TWO);
    }

    @After
    public void afterClear() throws AbstractException {
        service.clear();
        userService.clear();
    }

    @Test
    public void When_FindByIdExistsSESSION_Expect_ReturnSession() throws AbstractException {
        @Nullable final SessionDTO session = service.findById(SESSION_USER_ONE.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(SESSION_USER_ONE.getUserId(), session.getUserId());
    }

    @Test
    public void When_FindByIdExistsSESSION_Expect_ThrowsEntityNotFoundException() {
        Assert.assertThrows(EntityNotFoundException.class,
                () -> service.findById(SESSION_ID_NOT_EXISTED)
        );

    }

    @Test
    public void When_SaveNotNullSession_Expect_ReturnSession() throws AbstractException {
        @NotNull final SessionDTO savedSession = service.save(SESSION_ADMIN_ONE);
        Assert.assertNotNull(savedSession);
        Assert.assertEquals(SESSION_ADMIN_ONE, savedSession);
        @Nullable final SessionDTO session = service.findById(SESSION_ADMIN_ONE.getId());
        Assert.assertNotNull(session);
        Assert.assertEquals(SESSION_ADMIN_ONE.getId(), session.getId());
        Assert.assertEquals(SESSION_ADMIN_ONE.getUserId(), session.getUserId());
        Assert.assertEquals(SESSION_ADMIN_ONE.getCreated(), session.getCreated());
        Assert.assertEquals(SESSION_ADMIN_ONE.getDate(), session.getDate());
        Assert.assertEquals(SESSION_ADMIN_ONE.getRole(), session.getRole());
    }

    @Test
    public void When_CountCommonUserSessions_Expect_ReturnTwo() throws AbstractException {
        final int count = service.count();
        Assert.assertEquals(2, count);
    }

    @Test
    public void When_ExistsByIdExistedSession_Expected_ReturnTrue() throws AbstractException {
        final boolean exists = service.existsById(SESSION_USER_ONE.getId());
        Assert.assertTrue(exists);
    }

    @Test
    public void When_ExistsByIdNotExistedSession_Expected_ReturnFalse() throws AbstractException {
        final boolean exists = service.existsById(SESSION_ID_NOT_EXISTED);
        Assert.assertFalse(exists);
    }

    @Test
    public void When_ExistsByIdWithUserIdExistedSession_Expected_ReturnTrue() throws AbstractException {
        final boolean exists = service.existsById(SESSION_USER_ONE.getId());
        Assert.assertTrue(exists);
    }

    @Test
    public void When_ExistsByIdWithUserIdNotExistedSession_Expected_ReturnFalse() throws AbstractException {
        final boolean exists = service.existsById(SESSION_ID_NOT_EXISTED);
        Assert.assertFalse(exists);
    }

    @Test
    public void When_RemoveExistedSession_Expect_ReturnSession() throws AbstractException {
        Assert.assertNotNull(service.save(SESSION_ADMIN_TWO));
        service.removeById(SESSION_ADMIN_TWO.getId());
    }

    @Test
    public void When_RemoveNotSession_Expect_ThrowsEntityNotFoundException() throws AbstractException {
        Assert.assertThrows(EntityNotFoundException.class,
                () -> service.removeById(SESSION_NOT_EXISTED.getId())
        );
    }

    @Test
    public void When_RemoveAll_Expect_ZeroCountSessions() throws AbstractException {
        service.save(SESSION_ADMIN_ONE);
        service.save(SESSION_ADMIN_TWO);
        service.clear();
        Assert.assertEquals(0, service.count());
    }

    @Test
    public void When_RemoveByIdExistedSession_Expect_Session() throws AbstractException {
        Assert.assertNotNull(service.save(SESSION_ADMIN_TWO));
        service.removeById(SESSION_ADMIN_TWO.getId());
    }

}
